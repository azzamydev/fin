import ApplicationLogo from "@/Components/ApplicationLogo";
import DarkmodeSwitch from "@/Components/Commons/DarkmodeSwitch";
import { Link } from "@inertiajs/react";

export default function Guest({ children }) {
    return (
        <div className="min-h-screen relative flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 duration-150 dark:bg-slate-900">
            <div className="fixed top-5 right-5">
                <DarkmodeSwitch />
            </div>
            <div>
                <Link href="/">
                    <ApplicationLogo className="w-20 h-20 fill-current text-gray-500 dark:to-white" />
                </Link>
            </div>

            <div className="w-full sm:max-w-md mt-6 px-6 py-4 bg-white duration-150 dark:bg-slate-800 shadow-md overflow-hidden sm:rounded-lg">
                {children}
            </div>
        </div>
    );
}
