import Navbar from "@/Components/Partials/Navbar";
import Sidebar from "@/Components/Partials/Sidebar";
import { usePage } from "@inertiajs/react";

export default function Authenticated({ user, header, children }) {
    const { app } = usePage().props;
    return (
        <>
            <main className=" flex min-h-screen gap-3 p-3 bg-gray-100 duration-150 dark:bg-slate-900">
                <Sidebar />
                <div className="grow space-y-5 flex flex-col">
                    <Navbar />
                    <main className="grow">{children}</main>
                    <footer className="flex justify-center items-center">
                        <small className="text-gray-500">
                            © Copyright {app.year} - Azzamydev. All rights
                            reserved.{" "}
                        </small>
                    </footer>
                </div>
            </main>
        </>
    );
}
