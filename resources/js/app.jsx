import "react-tooltip/dist/react-tooltip.css";
import "../css/app.css";
import "./bootstrap";

import { createInertiaApp } from "@inertiajs/react";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
import { createRoot } from "react-dom/client";
import { useDarkmode } from "./Stores/darkmodeStore";

const appName = import.meta.env.VITE_APP_NAME || "Laravel";

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) =>
        resolvePageComponent(
            `./Pages/${name}.jsx`,
            import.meta.glob("./Pages/**/*.jsx")
        ),
    setup({ el, App, props }) {
        const root = createRoot(el);

        const Fin = () => {
            const darkmode = useDarkmode((state) => state.theme);

            if (darkmode) {
                document.body.classList.add("dark");
            } else {
                document.body.classList.remove("dark");
            }

            return <App {...props} />;
        };
        root.render(<Fin />);
    },
    progress: false,
});
