export default function Checkbox({ className = '', ...props }) {
    return (
        <input
            {...props}
            type="checkbox"
            className={
                'rounded border-gray-300 dark:bg-slate-700 dark:border-slate-500 duration-100 text-primary-600 shadow-sm focus:ring-primary-500 ' +
                className
            }
        />
    );
}
