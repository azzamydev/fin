import { forwardRef, useEffect, useRef, useState } from "react";
import { EyeFill, EyeSlashFill } from "react-bootstrap-icons";

export default forwardRef(function TextInput(
    { type = "text", className = "", isFocused = false, ...props },
    ref
) {
    const input = ref ? ref : useRef();
    const [openEye, setOpenEye] = useState(false);

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    if (type == "password") {
        return (
            <div className="relative">
                <button
                    onClick={() => setOpenEye((state) => !state)}
                    type="button"
                    className="absolute inset-y-0 right-3 text-slate-500 dark:text-slate-300"
                >
                    {openEye ? (
                        <EyeFill size={16} />
                    ) : (
                        <EyeSlashFill size={16} />
                    )}
                </button>
                <input
                    {...props}
                    type={openEye ? "text" : "password"}
                    className={
                        "border-gray-300 duration-100 dark:bg-slate-700 dark:text-white text-slate-700 dark:border-gray-600 focus:border-primary-500 focus:ring-primary-500 rounded-md shadow-sm " +
                        className
                    }
                    ref={input}
                />
            </div>
        );
    }
    return (
        <input
            {...props}
            type={type}
            className={
                "border-gray-300 duration-100 dark:bg-slate-700 dark:text-white text-slate-700 dark:border-gray-600 focus:border-primary-500 focus:ring-primary-500 rounded-md shadow-sm " +
                className
            }
            ref={input}
        />
    );
});
