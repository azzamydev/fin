import { Transition } from "@headlessui/react";
import { Link, usePage } from "@inertiajs/react";
import React, { Fragment, useState } from "react";
import { DoorOpenFill, GearFill, PersonCircle } from "react-bootstrap-icons";

const ProfileDropdown = () => {
    const { auth } = usePage().props;
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className="relative">
            <img
                onClick={() => setIsOpen((state) => !state)}
                src="https://picsum.photos/id/16/300/300"
                alt="user"
                className="rounded-full w-[40px] h-[40px] hover:cursor-pointer"
            />
            <div
                onClick={() => setIsOpen(false)}
                className={`inset-0 z-40 fixed ${!isOpen && "hidden"}`}
            ></div>
            <Transition
                as={Fragment}
                show={isOpen}
                enter="transition ease-out duration-200"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
            >
                <div className="min-w-[250px] text-sm flex flex-col rounded-lg overflow-hidden right-0 top-[45px] absolute z-50 bg-white dark:bg-slate-800 shadow-md border dark:border-slate-700">
                    <span className="text-slate-700 dark:text-white text-center px-4 py-2 border-b dark:border-b-slate-700">
                        {auth.user.name}
                    </span>
                    <ul className="w-full text-slate-600">
                        <Submenu
                            href={route("profile.edit")}
                            icon={<PersonCircle size={16} />}
                            label={"Profile"}
                        />
                        <Submenu
                            icon={<GearFill size={16} />}
                            label={"Settings"}
                        />
                        <Submenu
                            href={route("logout")}
                            method="post"
                            as="button"
                            className={"hover:bg-red-500"}
                            icon={<DoorOpenFill size={16} />}
                            label={"Logout"}
                        />
                    </ul>
                </div>
            </Transition>
        </div>
    );
};

const Submenu = ({ link, icon, label, className, ...props }) => {
    return (
        <li>
            <Link
                {...props}
                className={`fill-slate-600 w-full hover:bg-primary-500 hover:text-white dark:text-white flex items-center gap-x-3 font-medium px-3 py-2.5 duration-100 ${className}`}
            >
                {icon} {label}
            </Link>
        </li>
    );
};

export default ProfileDropdown;
