import { useSidebar } from "@/Stores/sidebarStore";
import { List } from "react-bootstrap-icons";
import ProfileDropdown from "./ProfileDropdown";
import Notification from "./Notification";
import DarkmodeSwitch from "../Commons/DarkmodeSwitch";

const Navbar = () => {
    const sidebarToggle = useSidebar((state) => state.toggle);

    return (
        <nav className="w-full p-3 flex justify-between items-center">
            <button className="icon" onClick={() => sidebarToggle()}>
                <List size={28} />
            </button>
            <div className="flex items-center gap-x-8">
                <DarkmodeSwitch />
                <Notification  />
                <ProfileDropdown />
            </div>
        </nav>
    );
};

export default Navbar;
