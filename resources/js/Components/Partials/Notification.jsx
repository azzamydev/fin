import { Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { BellFill } from "react-bootstrap-icons";

const data = [
    {
        title: "Lorem, ipsum dolor.",
        description:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat, sint!",
        isRead: false,
    },
    {
        title: "Lorem, ipsum dolor.",
        description:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat, sint!",
        isRead: true,
    },
    {
        title: "Lorem, ipsum dolor.",
        description:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat, sint!",
        isRead: false,
    },
    {
        title: "Lorem, ipsum dolor.",
        description:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat, sint!",
        isRead: false,
    },
];

const Notification = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [hasNotification, setHasNotification] = useState(false);

    return (
        <div className="relative">
            <div className="relative">
                <span
                    className={`relative right-0 top-2 flex h-3 w-3 justify-center items-center ${
                        !hasNotification && "hidden"
                    }`}
                >
                    <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-success-400 opacity-75"></span>
                    <span className="relative inline-flex rounded-full h-2 w-2 bg-success-500"></span>
                </span>
                <button onClick={() => setIsOpen((state) => !state)}>
                    <BellFill
                        size={20}
                        className="hover:cursor-pointer icon hover:text-primary-500 duration-200"
                    />
                </button>
            </div>
            <div
                onClick={() => setIsOpen(false)}
                className={`inset-0 z-40 fixed ${!isOpen && "hidden"}`}
            ></div>
            <Transition
                as={Fragment}
                show={isOpen}
                enter="transition ease-out duration-200"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
            >
                <div className="min-w-[350px] flex dark:text-white flex-col rounded-lg overflow-hidden right-0 top-[30px] absolute z-50 bg-white dark:bg-slate-800 shadow-md border dark:border-slate-800">
                    <span className="bg-slate-700 text-white text-start font-bold px-4 py-2">
                        Notification
                    </span>
                    <ul
                        className={`w-full text-slate-600 dark:text-white/90 cursor-pointer`}
                    >
                        {data.map((item, i) => {
                            return (
                                <li
                                    key={i}
                                    className={`px-3 py-2 border-b dark:border-b-slate-700 ${
                                        !item.isRead
                                            ? "hover:bg-primary-50 dark:hover:bg-slate-900"
                                            : "bg-gray-100 dark:bg-slate-900"
                                    }`}
                                >
                                    <p className="font-bold text-sm">
                                        {item.title}
                                    </p>
                                    <p className="text-xs text-slate-500 dark:text-white/70">
                                        {item.description.substring(0, 40)}
                                    </p>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </Transition>
        </div>
    );
};

export default Notification;
