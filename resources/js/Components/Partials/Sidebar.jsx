import { useSidebar } from "@/Stores/sidebarStore";
import { usePage } from "@inertiajs/react";
import {
    BarChartLineFill,
    Cart4,
    HouseFill,
    PersonCircle,
} from "react-bootstrap-icons";
import ApplicationLogo from "../ApplicationLogo";
import Menu from "./Menus/Index";
import NavLink from "./Menus/NavLink";
import CollapsedNav from "./Menus/CollapsedNav";

const Sidebar = () => {
    const open = useSidebar((state) => state.open);
    const { app } = usePage().props;
    return (
        <aside
            className={`flex transition-all duration-300 sticky top-4 h-[97vh] flex-col items-center justify-start gap-y-5 bg-slate-900 py-3 text-white dark:bg-slate-800 ${
                open ? "min-w-[260px] rounded-2xl" : "min-w-[70px] rounded-lg"
            }`}
        >
            <div className="flex items-center justify-center gap-x-3 my-1">
                <ApplicationLogo className="w-10 h-10 fill-white" />
                <span
                    className={`font-bold text-xl duration-150 ${
                        !open && "hidden"
                    }`}
                >
                    {app.name}
                </span>
            </div>
            <Menu>
                {/* Dashboard */}
                <NavLink
                    link="dashboard"
                    label="Dashboard"
                    icon={<HouseFill size={22} />}
                />
                {/* Profile */}
                <NavLink
                    link="profile.edit"
                    label="Profile"
                    icon={<PersonCircle size={22} />}
                />
                {/* Exmple Navigation Dropdown */}
                <CollapsedNav label="Reports" routeNamePrefix="reports">
                    <CollapsedNav.Item
                        link="reports.revenue"
                        label="Revenue"
                        icon={<BarChartLineFill size={22} />}
                    />
                    <CollapsedNav.Item
                        link="reports.purchase"
                        label="Purchase"
                        icon={<Cart4 size={22} />}
                    />
                </CollapsedNav>
            </Menu>
        </aside>
    );
};

export default Sidebar;
