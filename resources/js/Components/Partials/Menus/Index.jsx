import React from 'react'

const Menu = ({children, className}) => {
  return (
    <ul className={`w-full space-y-3 overflow-y-auto px-2 pb-5 ${className}`}>
      {children}
    </ul>
  )
}

export default Menu