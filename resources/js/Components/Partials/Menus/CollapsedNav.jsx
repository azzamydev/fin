import { useSidebar } from "@/Stores/sidebarStore";
import { Link } from "@inertiajs/react";
import { useEffect } from "react";
import { ChevronDown } from "react-bootstrap-icons";
import { useCollapse } from "react-collapsed";
import { Tooltip } from "react-tooltip";

const createState = (key) => {
    if (localStorage.getItem(key) == undefined) {
        localStorage.setItem(key, JSON.stringify(false));
    }
};

const setDropdownState = (key, value) => {
    if (localStorage.getItem(key) !== undefined) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};

const getDropdownState = (key) => {
    return JSON.parse(localStorage.getItem(key));
};

const CollapsedNav = ({ label, children, routeNamePrefix = "" }) => {
    createState(routeNamePrefix);

    const open = useSidebar((state) => state.open);
    const active = route().current(routeNamePrefix + "*") ?? false;

    const { getCollapseProps, getToggleProps, isExpanded, setExpanded } =
        useCollapse({
            defaultExpanded: !open ? true : getDropdownState(routeNamePrefix),
            onTransitionStateChange: () => {
                setDropdownState(routeNamePrefix, isExpanded);
            },
        });

    useEffect(() => {
        if (!open) {
            setExpanded(true);
            setDropdownState(routeNamePrefix, true);
        } else {
            if (!active) {
                setExpanded(!getDropdownState(routeNamePrefix));
            }
        }
    }, [open]);

    useEffect(() => {
        if (active) {
            setExpanded(true);
            setDropdownState(routeNamePrefix, true);
        } else {
            setExpanded(getDropdownState(routeNamePrefix));
        }
    }, [active]);

    return (
        <div className="">
            <div
                {...getToggleProps()}
                className={`flex px-4 py-2 gap-x-3 group rounded-lg duration-200 hover:bg-slate-800 justify-start items-center ${
                    open ? "block" : "hidden"
                }`}
            >
                <ChevronDown
                    size={18}
                    className={`duration-200 transition-all ${
                        isExpanded ? "rotate-180" : "rotate-0"
                    }`}
                />
                <span className="font-medium capitalize group-hover:translate-x-5 duration-500">
                    {label}
                </span>
            </div>
            <ul {...getCollapseProps()} className="py-2 space-y-2 list-disc">
                {children}
            </ul>
        </div>
    );
};

const Item = ({
    link = "",
    className = "",
    label = "",
    icon = null,
    ...props
}) => {
    const isOpen = useSidebar((state) => state.open);
    const active = route().current(link + "*") ?? false;
    return (
        <>
            <Link
                {...props}
                href={route(link)}
                data-tooltip-id={link}
                className={
                    `w-full flex items-center gap-x-3 font-medium delay-100 rounded-lg px-3 py-2 duration-200 text-white/80 ${
                        active
                            ? "bg-primary-500 text-white"
                            : "hover:bg-slate-800 hover:text-white dark:hover:bg-slate-900"
                    } ${isOpen ? "justify-start" : "justify-center"}` +
                    className
                }
            >
                {icon}
                {isOpen && label}
            </Link>
            <Tooltip
                id={link}
                place="right"
                variant="light"
                hidden={isOpen}
                className="border shadow bg-white rounded-2xl"
            >
                {label}
            </Tooltip>
        </>
    );
};

CollapsedNav.Item = Item;

export default CollapsedNav;
