import { useSidebar } from "@/Stores/sidebarStore";
import { Link } from "@inertiajs/react";
import { Tooltip } from "react-tooltip";

export default function NavLink({
    link = "",
    className = "",
    label = "",
    icon = null,
    ...props
}) {
    const isOpen = useSidebar((state) => state.open);
    const active = route().current(link + "*");

    return (
        <>
            <Link
                {...props}
                href={route(link)}
                data-tooltip-id={link}
                className={
                    `w-full flex items-center gap-x-3 font-medium delay-100 rounded-lg px-3 py-2 duration-200 text-white/80 ${
                        active
                            ? "bg-primary-500 text-white"
                            : "hover:bg-slate-800 hover:text-white dark:hover:bg-slate-900"
                    } ${isOpen ? "justify-start" : "justify-center"}` +
                    className
                }
            >
                {icon}
                {isOpen && label}
            </Link>
            <Tooltip id={link} place="right" variant="light" hidden={isOpen} className="border shadow bg-white rounded-2xl">{label}</Tooltip>
        </>
    );
}
