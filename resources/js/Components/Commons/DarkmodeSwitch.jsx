import { useDarkmode } from "@/Stores/darkmodeStore";
import React from "react";
import { MoonStarsFill, SunFill } from "react-bootstrap-icons";

const DarkmodeSwitch = () => {
    const theme = useDarkmode((state) => state.theme);
    const darkmodeToggle = useDarkmode((state) => state.toggle);
    const classname = "text"
    return (
        <button className="icon hover:text-primary-500" onClick={() => darkmodeToggle()}>
            {theme ? <MoonStarsFill size={20} /> : <SunFill size={20} />}
        </button>
    );
};

export default DarkmodeSwitch;
