import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";

export const useDarkmode = create(
    persist(
        (set, get) => ({
            theme: false,
            toggle: () => {
                set({ theme: !get().theme });
            },
        }),
        {
            name: "darkmode", // name of the item in the storage (must be unique)
            storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
        }
    )
);
