import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";

export const useDropdown = create(
    persist(
        (set, get) => ({
            open: false,
            toggle: () => {
                set({ open: !get().open });
            },
        }),
        {
            storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
        }
    )
);
