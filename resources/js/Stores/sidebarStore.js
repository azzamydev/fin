import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";

export const useSidebar = create(
    persist(
        (set, get) => ({
            open: false,
            toggle: () => {
                set({ open: !get().open });
            },
        }),
        {
            name: "sidebar", // name of the item in the storage (must be unique)
            storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
        }
    )
);
