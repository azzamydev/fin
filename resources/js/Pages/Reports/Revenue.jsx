import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";

const Revenue = ({ auth }) => {
    return (
        <AuthenticatedLayout user={auth.user}>
            <div className="p-5 w-full bg-white dark:bg-slate-800 rounded-xl"></div>
        </AuthenticatedLayout>
    );
};

export default Revenue;
