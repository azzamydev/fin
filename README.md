<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="300" alt="Laravel Logo"></a></p>

<h2 style="color:red">FIN Starterkit</h2>

<small>FIN is a starterkit based on Laravel Breeze, where we have changed the dashboard appearance with a new one.</small>


#### Instalation

```bash
git clone https://gitlab.com/azzamydev/fin.git
cd fin
composer install
cp .env.example .env
php artisan key:generate
bun i && bun run build
php artisan migrate
```